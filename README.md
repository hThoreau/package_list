# package_list
> A plugin for [Oh My Fish](https://www.github.com/oh-my-fish/oh-my-fish).

[![GPL License](https://img.shields.io/badge/license-GPL-blue.svg?longCache=true&style=flat-square)](/LICENSE)
[![Fish Shell Version](https://img.shields.io/badge/fish-v2.7.1-blue.svg?style=flat-square)](https://fishshell.com)
[![Oh My Fish Framework](https://img.shields.io/badge/Oh%20My%20Fish-Framework-blue.svg?style=flat-square)](https://www.github.com/oh-my-fish/oh-my-fish)

<br/>


## Description

A simple utility to keep track of manually installed packages as you install them. It is especially useful when using Tails' "[Additional software packages](https://tails.boum.org/doc/first_steps/persistence/configure/index.en.html#additional_software)" feature.

## Options

```
apt -a/--add [package] ...
Install packages and add them to the package list. If the list does\'t exist, create it with the entries provided.

apt -r/--remove [package] ...
Uninstall packages and remove them from the package list.

apt -f/--file [tails] [directory]
Set the directory to place the package list file. by specifying "tails" puts it at Tails' default listing location for Additional Software Packages.

apt -l/--list
Print the contents of the package list.

apt -h/--help
Display these instructions.
```

## Example usage

[![asciicast](https://asciinema.org/a/BrVczA8uVa5tdVdEbbvCgBqfo.png)](https://asciinema.org/a/BrVczA8uVa5tdVdEbbvCgBqfo)

## Install

Either with omf
```fish
omf install package_list
```
or [fisherman](https://github.com/fisherman/fisherman)
```fish
fisher i gitlab.com/lusiadas/package_list
```

___

Ⓐ Made in Anarchy. No wage slaves were economically coerced into the making of this work.
